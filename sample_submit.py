#!/usr/bin/env python3

import os
import sys
import time
import random
import argparse

cwd = os.getcwd()
eos = "/eos/cms/store/group/dpg_tracker_upgrade/IT_data_rates/"
path_proxy = f"{cwd}/tmp/x509up"

process_cfis = {
    'ttbar' : 'TTbar_14TeV_TuneCP5_cfi',
    'fourmu' : 'FourMuExtendedPt_1_200_pythia8_cfi',
    'minbias' : 'MinBias_14TeV_pythia8_TuneCP5_cfi'
}

def main(args) :

    njobs = args.njobs
    nevents = args.nevents
    process_cfi = process_cfis[args.process]
    process = args.process
    tag = args.tag
    pileup_input = args.pileup_input
    random_seed = args.random_seed
    config_file = args.config_file
    full_name = process + "_" + config_file.split("/")[-1].split(".")[0]
    lumi_block = int(random_seed)*1000

    path_dir = f"{cwd}/work/{full_name}/{tag}/"
    path_eos = f"{eos}/{full_name}/{tag}/"

    for ijob in range(0, njobs) : 

        job = f"run_{str(ijob).zfill(3)}"
        config_file_job = f"{path_dir}/{job}/run.sh"
        condor_file_job = f"{path_dir}/{job}/condor.jds"

        random_seed = lumi_block + ijob
        os.system(f"mkdir -p {path_dir}/{job}/")
        os.system(f"mkdir -p {path_eos}/{job}/")
        os.system(f"cp {config_file} {config_file_job}")

        os.system(f"sed -i 's|__process_cfi__|{process_cfi}|g' {config_file_job}")
        os.system(f"sed -i 's|__nevents__|{nevents}|g' {config_file_job}")
        os.system(f"sed -i 's|__random_seed__|{random_seed}|g' {config_file_job}")

        pileup_files = ""
        if "local" in pileup_input :
            with open(pileup_input) as f :
                pileup_input_lines = f.readlines()
                for pileup_input_line in pileup_input_lines :
                    pileup_files = f"{pileup_files},file:{pileup_input_line.strip()}"
            pileup_files = pileup_files[1:]
        else :
            with open(pileup_input) as f :
                pileup_input_lines = f.readlines()
                for pileup_input_line in pileup_input_lines :
                    pileup_files = f"{pileup_files},das:{pileup_input_line.strip()}"
            pileup_files = pileup_files[1:]
#        else :
#            sys.exit(f"{pileup_input} should either have 'local' or 'relval' specified in their file name")

        os.system(f"sed -i 's|__pileup_files__|{pileup_files}|g' {config_file_job}")

        #FIXME need to change MinBias to be xrdcp-ed with step1.root not step2.root
        xrdcp_command = fr"xrdcp step2.root root://eosuser.cern.ch/{path_eos}/{job}/"
        os.system(f"sed -i 's|__xrdcp_command__|{xrdcp_command}|g' {config_file_job}")

        os.system(f"cp template/condor.jds {condor_file_job}")
        os.system(f"sed -i 's|__process__|{process}|g' {condor_file_job}")
        os.system(f"sed -i 's|__tag__|{tag}|g' {condor_file_job}")
        os.system(f"sed -i 's|__path_proxy__|{path_proxy}|g' {condor_file_job}")

        os.chdir(f"{path_dir}/{job}")
        os.system(f"condor_submit condor.jds")
        os.chdir(cwd)

def get_argparse() :

    parser = argparse.ArgumentParser(description='sample geneation')

    parser.add_argument('-j', dest='njobs', default=1, type=int,
        help='number of jobs to submit to condor')

    parser.add_argument('-n', dest='nevents', default=10, type=int,
        help='number of events per job to submit')

    parser.add_argument('-p', dest='process', default='ttbar', 
        help='physics process to generate')

    parser.add_argument('-t', dest='tag', default=str(time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime())),
        help='tag for the samples')

    parser.add_argument('--pileup_input', required=True,
        help='minbias sample to embed') # FIXME change to private pileup flag

    parser.add_argument('--random_seed', default=int(random.random()*1000),
        help='random seed to inject')

    parser.add_argument('config_file',
        help='CMSSW and geometry template to use')

    args = parser.parse_args()

    assert (type(args.njobs) == int) # force integer for njobs
    assert (type(args.nevents) == int) # force integer for nevents
    assert (args.process in list(process_cfis.keys())) # force process to be in process_cfis
    assert (type(args.tag) == str) # force string for tag
    assert ((args.pileup_input == None and args.process == 'minbias') or (args.pileup_input != None and args.process != 'minbias')) # force no pileup embedded for minbias sample # force pileup embedded for minbias sample
    assert (type(args.random_seed) == int) # force integer for random seed
    assert (os.path.exists(args.config_file)) # check whether configuration file template exists or not

    return args 

if __name__ == "__main__" :

    args = get_argparse()
    main(args)
